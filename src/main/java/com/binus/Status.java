package com.binus;

    public enum Status {
        DONE("DONE"),
        NOT_DONE("NOT DONE");

        private String displayName;

        Status(String displayName) {
            this.displayName = displayName;
        }

        @Override
        public String toString() {
            return displayName;
        }
    }
