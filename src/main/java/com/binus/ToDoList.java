package com.binus;

import java.util.ArrayList;

public class ToDoList {

    private ArrayList <ToDo> taskList;
    public ToDoList() {
        this.taskList = new ArrayList<>();
        this.taskList.add(new ToDo(1,"Budi",Status.NOT_DONE));
        this.taskList.add(new ToDo(2,"Charlie",Status.NOT_DONE));
        this.taskList.add(new ToDo(3,"Curry",Status.DONE));
    }

    public String getTaskById(int id) {
        for (ToDo task: taskList){
            if (task.getId() == id) return task.getId()+". "+task.getName()+" ["+task.getStatus()+"]";
        }
        return null;
    }

    public String getAllTask() {
        String collection = "";
        int length = taskList.size();
        for(int a = 0;a< length; a++){
            ToDo task = taskList.get(a);
            if (a==length-1) collection+= task.getId()+". "+task.getName()+" ["+task.getStatus()+"]";
            else collection += task.getId()+". "+task.getName()+" ["+task.getStatus()+"]\n";
        }
//        for (Task task: taskList){
//            collection+=task.getId()+". "+task.getName()+" ["+task.getStatus()+"]\n";
//        }
        return collection;
    }

}
