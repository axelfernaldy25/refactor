package com.binus;

public class ToDo {
    private String name;
    private int  id ;
    private Status status;

    public ToDo(){}
    public ToDo(int id, String name,  Status status) {
        this.name = name;
        this.id = id;
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}
