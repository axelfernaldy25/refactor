package com.binus;

import org.junit.Test;

import static org.junit.Assert.*;

public class ToDoListTest {

    @Test
    public void taskReturnedIsNullTest(){
        ToDoList todo = new ToDoList();
        String task = todo.getTaskById(0);
        assertNull(task);
    }
    @Test
    public void taskReturnedIsNotNullTest(){
        ToDoList todo = new ToDoList();
        String task = todo.getTaskById(1);
        assertNotNull(task);
    }
    @Test
    public void taskReturnedIsCorrectTest(){
        ToDoList todo = new ToDoList();
        String task = todo.getTaskById(1);
        assertEquals("1. Budi [NOT DONE]",task);
        task = todo.getTaskById(2);
        assertEquals("2. Charlie [NOT DONE]",task);
        task = todo.getTaskById(3);
        assertEquals("3. Curry [DONE]",task);
    }
    @Test
    public void taskListIsNotNull(){
        ToDoList todo = new ToDoList();
        assertNotNull(todo);
    }
    @Test
    public void taskListGetAllIsCorrect(){
        ToDoList todo = new ToDoList();
        assertEquals("1. Budi [NOT DONE]\n2. Charlie [NOT DONE]\n3. Curry [DONE]", todo.getAllTask());
    }

}