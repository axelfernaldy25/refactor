package com.binus;

import org.junit.Test;

import static org.junit.Assert.*;

public class ToDoTest {

    @Test
    public void convertAsset() {
        ToDo task = new ToDo(1, "Do Dishes",Status.DONE);
        ToDo expectedTask = new ToDo();

        expectedTask.setName("Do Dishes");
        expectedTask.setId(1);

        expectedTask.setStatus(Status.DONE);
        assertEquals(expectedTask.getId(),task.getId());
        assertEquals(expectedTask.getName(),task.getName());
        assertEquals(expectedTask.getStatus(),task.getStatus());

    }

    @Test
    public void taskIdIsSetAndGet(){
        ToDo task = new ToDo();
        int expectedId = 1;
        task.setId(expectedId);
        assertEquals(expectedId,task.getId());
    }


    @Test
    public void taskNameIsSetAndGet(){
        ToDo task = new ToDo();
        String expectedName = "Belajar";
        task.setName(expectedName);
        assertEquals(expectedName,task.getName());
    }

    @Test
    public void taskStatusIsGetAndSet(){
        ToDo task = new ToDo();
        Status expectedStatus = Status.DONE;
        task.setStatus(expectedStatus);
        assertEquals(expectedStatus,task.getStatus());
    }

}